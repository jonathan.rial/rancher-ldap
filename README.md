# Rancher LDAP

Rancher has two issues blocking its use at HEIA-FR:

* The LDAP integration doesn't re-bind the service account to search for user rights.
* The LDAP integration doesn't use the right cipher for TLS encryption.

To fix these issues, we have to modify the code and compile it.

## Fix service account re-bind

To fix the service account re-bind issue, open the `pkg/auth/providers/ldap/ldap_client.go` file with your favorite editor and add these lines:

```diff
  userDN := result.Entries[0].DN //userDN is externalID
  err = lConn.Bind(userDN, password)
  if err != nil {
      if ldapv3.IsErrorWithCode(err, ldapv3.LDAPResultInvalidCredentials) {
          return v3.Principal{}, nil, httperror.WrapAPIError(err, httperror.Unauthorized, "authentication failed: invalid credentials")
      }
      return v3.Principal{}, nil, httperror.WrapAPIError(err, httperror.ServerError, "server error while authenticating")
  }

+ /* Rebind as read-only / bind user account for further queries */
+ err = ldap.AuthenticateServiceAccountUser(serviceAccountPassword, serviceAccountUserName, "", lConn)
+ if err != nil {
+     return v3.Principal{}, nil, err
+ }

  searchOpRequest := ldap.NewWholeSubtreeSearchRequest(
      userDN,
      fmt.Sprintf("(%v=%v)", ObjectClass, config.UserObjectClass),
      operationalAttrList,
  )
```

The original patch is available [here](https://github.com/rancher/rancher/pull/34951).

## Fix the cipher

To define the right cipher for the TLS encryption, open the `pkg/auth/providers/common/ldap/ldap_util.go` with your editor and add this flag:

```diff
- tlsConfig = &tls.Config{RootCAs: caPool, InsecureSkipVerify: false, ServerName: server}
+ tlsConfig = &tls.Config{RootCAs: caPool, InsecureSkipVerify: false, ServerName: server, CipherSuites: []uint16{tls.TLS_RSA_WITH_AES_256_CBC_SHA}}
```

## Compile from source

To compile from source, open the `dev-scripts/quick` file with your editor and modify the `TAG` variable with the Rancher's version (for example `v2.9.1`):

```diff
- TAG="${TAG:-$(yq '.env.TAG | sub("-.*", "")' < .github/workflows/pull-request.yml)-${COMMIT}}"
+ TAG="v2.9.1"
```

Then, you can run the script from the project's root folder:

```sh
./dev-scripts/quick
```

A container image named `rancher/rancher` will be added to your local container image registry. You can re-tag it with the right name for the HEFR GitLab Registry and push the image to the registry.